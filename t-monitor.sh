#!/bin/bash
t=$(journalctl --boot -xe | grep -i temperature | tail -1 | awk '{print $(NF-1)}')
t_lim=60
mail_to="somov.i.v@yandex.ru"

if [ $t -gt $t_lim ]
then
    echo "T > $t_lim ($t) send an email"
    echo -e "limit $t_lim*C \n$(journalctl --boot -xe | grep -i temperature | tail -1)" | mail -s "$HOSTNAME $t *C" -r t-monitor@playkey.ru $mail_to
fi