#!/bin/bash
repos=$(docker images --format "{{.Repository}}:{{.Tag}}" | grep -v name_repo)
for repo in $repos
  do
    docker tag $repo name_repo:5001/$repo
    docker push name_repo:5001/$repo
  done

