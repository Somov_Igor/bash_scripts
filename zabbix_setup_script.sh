#!/bin/bash
yum install zabbix50-agent.x86_64 -y

mkdir -p /export/home/zabbix && chown zabbix:zabbix /export/home/zabbix && chmod 700 /export/home/zabbix && usermod -s /bin/bash -d /export/home/zabbix zabbix

mkdir -p /etc/zabbix && touch /etc/zabbix/zabbix_agentd.conf

touch /var/log/zabbix_agentd.log && chown zabbix:zabbix /var/log/zabbix_agentd.log

cat > /etc/zabbix/zabbix_agentd.conf << EOF
Server=xxx.xxx.xxx.xxx
ServerActive=xxx.xxx.xxx.xxx
HostMetadata=Linux 21df83bf21bf0be66312kh36k498558ab9b95fba66a6dbf834f8b91ae5e08ae
LogFile=/var/log/zabbix_agentd.log
EOF

cat > /usr/lib/systemd/system/zabbix-agent.service << EOF
[Unit]
Description=Zabbix Monitor Agent
After=syslog.target network.target

[Service]
Type=simple
ExecStart=/usr/sbin/zabbix_agentd -f -c /etc/zabbix/zabbix_agentd.conf
User=zabbix

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload

chmod 755 /etc/zabbix && chown -R root:zabbix /etc/zabbix/* && chmod 660 /etc/zabbix/zabbix_agentd.conf && chmod 775 /usr/sbin/zabbix_agentd

mkdir -p /usr/lib/zabbix/scripts && chown root:zabbix /usr/lib/zabbix/scripts && chmod 775 /usr/lib/zabbix/scripts

echo '%zabbix ALL=(root) NOPASSWD: /usr/bin/systemctl * zabbix-agent.service' >> /etc/sudoers

systemctl enable zabbix-agent
systemctl restart zabbix-agent
systemctl status zabbix-agent