#!/bin/bash
top -bn1 | grep load | awk '{printf "CPU Load: %.2f\n", $(NF-2)}'
free -g | awk 'NR==2{printf "Memory Usage: %s/%sGB (%.2f%%)\n", $3,$2,$3*100/$2 }'
free -g | awk 'NR==3{printf "Swap Usage: %s/%sGB (%.2f%%)\n", $3,$2,$3*100/$2 }'
df -h | awk '$NF=="/"{printf "System Disk Usage: %d/%dGB (%s)\n", $3,$2,$5}'
df -h | awk '$NF=="/data"{printf "Data Disk Usage: %d/%dGB (%s)\n", $3,$2,$5}'
echo "_________________________________________"
echo
container_names=$(docker container ls --format '{{.Names}}')
services=$(ls /data/installs/ | grep ".service")
for service in $services
  do
    status=$(systemctl is-active $service)
      echo "______ $service ______"
      echo "$service - $status"
      echo
  done

for container_name in $container_names
  do
    echo "______ $container_name ______"
    docker ps --filter "name=$container_name"
    echo
  done
